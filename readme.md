# Power Run
A First Person Shooter.
Battle Royale in an automatically generated world.

# Quickstart to edit the game
This is how you can start editing Power-Run:

1. Download [the game repository](https://codeberg.org/Power-Run/Power-Run/archive/main.zip).
2. Download the latest [Godot](https://godotengine.org/) binary from the website and place it in the main folder.
3. Start the Godot binary and run the "Power Run" project.