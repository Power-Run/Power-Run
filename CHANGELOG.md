# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.4] - 2024-6-27
### Added 
- UI: set number of sides and biome pool
- UI: Loading progress bar
- UI: keymap, version and homepage URL on splash screen
- Animated background with random geometry
- now there are 100 Biomes and 10 Building types
- pressing "Q" flips gravity towards viewing direction
- pressing "esc" always quits the game (even in menu)
- readme.md with basic info about the project

### Changed
- Player tries to spawn over solid ground
- sides don't "touch" each other correctly anymore
- environment: fog, sky and sun
- tiles generally have a flat down side 

### Removed
- temporarily disabled camera Auto Exposure
- UI: debug info for biomes 

## [0.1.3] - 2023-10-2
### Added
- functionality: minitile variation 
- new biomes
- new biome variations
- fixes for biomes
- debug info for biomes in the GUI

## [0.1.2] - 2023-1-26
### Added
- it is possible to randomly generate a world
- option to create world by seed
- simple start menu
- new buildings are placed randomly
- lots of new biomes
- background sound

## Changed
- textures look better

### Removed
- switcher has been removed (press 1-6 for planes)
- half of the cube is removed, only 3 planes remain

## [0.1.1] - 2022-10-2
### Added 
- proper physics for player movement 
- a manually generated 512x cube world
- 11 biomes with complete mondrian tile sets
- an atmosphere with 2 suns, 2 omni lights and fog
- smooth plane switching
- differently colored biome textures
- a splash screen

### Removed
- basic level for testing purposes

## [0.1.0] - 2022-7-16
### Added 
- This CHANGELOG file
- a basic level to test the FPS controller
- a simple FPS controller that changes planes when pressing 1,2,3,4,5,6
- a plain grid texture
- an icon

## [0.1.0] - 2022-05-11
### Added





