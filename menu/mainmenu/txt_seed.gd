extends LineEdit

@export var SETTING_NAME : String = ""
@export var SETTING_DEFAULT : String = ""

var settings : ConfigFile
var words : Array


func set_random_seed() -> void:
	if not words:
		var words_config : ConfigFile = ConfigFile.new()
		words_config.load("res://resources/eff_large_wordlist.txt")
		words = words_config.get_value("GUI", "words")
	self.text = "-".join([words.pick_random(), words.pick_random(), words.pick_random()])


func _load_setting() -> void:
	#self.text = settings.get_value("GUI", SETTING_NAME, SETTING_DEFAULT)
	self.set_random_seed()


func _save_setting() -> void:
	settings.set_value("GUI", SETTING_NAME, self.text)


#func _enter_tree() -> void:


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	root_scene.save_settings.connect(self._save_setting)
	root_scene.load_settings.connect(self._load_setting)
