extends Button

@onready var vboxBiomeList : VBoxContainerBiomeList = %vboxBiomeList


func _pressed():
	match(self.name):
		"btnSelectAll":
			for button in vboxBiomeList.get_children():
				button.button_pressed = true
		"btnSelectNone":
			for button in vboxBiomeList.get_children():
				button.button_pressed = false
		"btnInvert":
			for button in vboxBiomeList.get_children():
				button.button_pressed = not button.button_pressed
