extends Button

@onready var txt_seed : LineEdit = %txtSeed

func _pressed() -> void:
	txt_seed.set_random_seed()
