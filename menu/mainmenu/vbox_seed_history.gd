extends VBoxContainer

var SETTING_SEED_HISTORY : String = "seed_history"
var SETTING_SEED_HISTORY_DEFAULT : Array = []

var settings : ConfigFile

@onready var txt_seed : LineEdit = %txtSeed


func _button_pressed(button : Button) -> void:
	txt_seed.text = button.text


func _load_setting() -> void:
	var seed_history : Array = settings.get_value("GUI", SETTING_SEED_HISTORY, SETTING_SEED_HISTORY_DEFAULT)
	for button in self.get_children():
		remove_child(button)
	for index in range(seed_history.size() - 1, -1, -1):
		var button : Button = Button.new()
		button.pressed.connect(_button_pressed.bind(button))
		button.text = seed_history[index]
		button.alignment = HORIZONTAL_ALIGNMENT_LEFT
		button.add_theme_font_size_override("font_size", 12)
		button.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		add_child(button)


func _save_setting() -> void:
	var seed_history : Array = settings.get_value("GUI", SETTING_SEED_HISTORY, SETTING_SEED_HISTORY_DEFAULT)
	seed_history.erase(txt_seed.text)
	seed_history.append(txt_seed.text)
	settings.set_value("GUI", SETTING_SEED_HISTORY, seed_history)
	#settings.save("user://settings.cfg") # FIXME


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	#root_scene.save_settings.connect(self._save_setting)
	root_scene.load_settings.connect(self._load_setting)
	var btn_start : Button = %btnStart
	btn_start.pressed.connect(self._save_setting)
