extends ProgressBar

var mutex : Mutex
var thread : Thread

var level : Level
var skybox : Node3D

func init_progress_bar(total : int):
	self.min_value = 0
	self.max_value = total
	self.step = 1


func update_progress_bar():
	self.value += 1


func _enter_tree() -> void:
	self.value = 0
	self.loaded = false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Global.load_block.connect(init_progress_bar)
	Global.load_block_inc.connect(update_progress_bar)


var loaded : bool = false
func _process(_delta: float) -> void:
	if not loaded and level and skybox and self.value == self.max_value:
		loaded = true
		thread.wait_to_finish()
		var f : Callable = $"/root/Game/".add_world_scenes.bind(level, skybox)
		get_tree().create_timer(0.2).timeout.connect(f)


func start_thread() -> void:
	# begin loading main scene
	mutex = Mutex.new()
	thread = Thread.new()
	thread.start(_thread_func)


func _thread_func() -> void:
	mutex.lock()
	level = load("res://world/level/level.tscn").instantiate()
	skybox = load("res://world/environment/skybox.tscn").instantiate()
	mutex.unlock()
