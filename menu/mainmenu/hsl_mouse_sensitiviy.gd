extends HSlider


var SETTING_MOUSE_SENSITIVITY : String = "mouse_sensitivity"
var SETTING_MOUSE_SENSITIVITY_DEFAULT : float = 0.05

var settings : ConfigFile


func _load_setting() -> void:
	self.value = settings.get_value("GUI", SETTING_MOUSE_SENSITIVITY, SETTING_MOUSE_SENSITIVITY_DEFAULT)


func _save_setting() -> void:
	settings.set_value("GUI", SETTING_MOUSE_SENSITIVITY, self.value)


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	root_scene.save_settings.connect(self._save_setting)
	root_scene.load_settings.connect(self._load_setting)
