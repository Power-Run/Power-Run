class_name ButtonStart
extends Button

var level : Level
var skybox : Node3D

var settings : ConfigFile
@onready var biome_list : VBoxContainerBiomeList = %vboxBiomeList
@onready var pbar_loading : ProgressBar = %pbarLoading


func _enter_tree() -> void:
	self.disabled = false


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	#root_scene.save_settings.connect(self._save_setting)
	#root_scene.load_settings.connect(self._load_setting)


func _pressed():
	self.disabled = true
	# possibly set some defaults
	if biome_list.get_selected_biome_names().is_empty():
		biome_list.get_child(0).button_pressed = true
	
	# save selected settings
	var root_scene : Game = $"/root/Game"
	root_scene.save_settings.emit()
	settings.save("user://settings.cfg")

	# start loading thread
	pbar_loading.start_thread()
