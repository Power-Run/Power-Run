class_name VBoxContainerBiomeList
extends VBoxContainer

var SETTING_BIOME_NAMES : String = "level_selected_biome_names"
var SETTING_BIOME_STATE : String = "biome_state_dict"
var SETTING_BIOME_NAMES_DEFAULT : Array = []
var SETTING_BIOME_STATE_DEFAULT : Dictionary = {}

var settings : ConfigFile


func get_selected_biome_names() -> Array:
	var biome_names : Array = []
	for button in self.get_children():
		if button.button_pressed == true:
			biome_names.append(button.text)
	return biome_names


func get_biome_state_dict() -> Dictionary:
	var biome_state_dict : Dictionary = {}
	for button in self.get_children():
		biome_state_dict[button.text] = button.button_pressed
	return biome_state_dict


func set_biome_state_dict(biome_state_dict : Dictionary):
	for button in self.get_children():
		if button.text in biome_state_dict:
			button.button_pressed = biome_state_dict[button.text]


func _load_setting() -> void:
	var biome_state_dict : Dictionary = settings.get_value("GUI", SETTING_BIOME_STATE, SETTING_BIOME_STATE_DEFAULT)
	self.set_biome_state_dict(biome_state_dict)


func _save_setting() -> void:
	settings.set_value("GUI", SETTING_BIOME_STATE, get_biome_state_dict())
	settings.set_value("GUI", SETTING_BIOME_NAMES, get_selected_biome_names())


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	root_scene.save_settings.connect(self._save_setting)
	root_scene.load_settings.connect(self._load_setting)
	# Load all biome names
	var biome_names : Array = Helper.get_subdirectory_list("res://world/biomes")
	for biome_name in biome_names:
		var button = CheckBox.new()
		button.text = biome_name
		button.button_pressed = true
		self.add_child(button)
