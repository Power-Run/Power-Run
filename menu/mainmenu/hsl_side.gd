extends HSlider
@export var SETTING_NAME : String = ""
@export var SETTING_DEFAULT : int = 0

var settings : ConfigFile


func _load_setting() -> void:
	self.value = settings.get_value("GUI", SETTING_NAME, SETTING_DEFAULT)


func _save_setting() -> void:
	settings.set_value("GUI", SETTING_NAME, self.value)


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	root_scene.save_settings.connect(self._save_setting)
	root_scene.load_settings.connect(self._load_setting)
