extends CheckButton


var SETTING_INVERT_MOUSE : String = "invert_mouse"
var SETTING_INVERT_MOUSE_DEFAULT : bool = false

var settings : ConfigFile


func _load_setting() -> void:
	self.button_pressed = settings.get_value("GUI", SETTING_INVERT_MOUSE, SETTING_INVERT_MOUSE_DEFAULT)


func _save_setting() -> void:
	settings.set_value("GUI", SETTING_INVERT_MOUSE, self.button_pressed)


func _ready() -> void:
	var root_scene : Game = $"/root/Game"
	self.settings = root_scene.settings
	root_scene.save_settings.connect(self._save_setting)
	root_scene.load_settings.connect(self._load_setting)
