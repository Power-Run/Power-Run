extends Node3D


func _set(property: StringName, value: Variant) -> bool:
	if property == "rotation":
		rotation = Vector3(clamp(value.x, -PI/2 + 0.1, PI/2 -0.1), value.y, value.z)
		return true
	return false
