class_name PlayerCamera
extends Camera3D


func _ready() -> void:
	pass # Replace with function body.


func _process(delta: float) -> void:
	# notify background camera
	updated.emit(self)

signal updated(camera : Camera3D)
