class_name DuckDuckState
extends PostureState


var DUCKDUCK_HEIGHT : float = 0.8


func perform_upright_to_duckduck() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", DUCKDUCK_HEIGHT, 0.16)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func perform_duck_to_duckduck() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", DUCKDUCK_HEIGHT, 0.08)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func _enter(prev_state_id: int) -> void:
	match prev_state_id:
		STATES.UPRIGHT:
			perform_upright_to_duckduck()
		STATES.DUCK:
			perform_duck_to_duckduck()


func _physics_process(delta: float) -> void:
	if not Input.is_action_pressed("crouch"):
		switch_to(STATES.UPRIGHT)
	elif state_machine.movement.state_id in [MovementState.STATES.CROUCHING]:
		switch_to(STATES.DUCK)
