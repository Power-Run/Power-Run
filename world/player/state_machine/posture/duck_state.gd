class_name DuckState
extends PostureState


var DUCK_HEIGHT : float = 0.95


func perform_upright_to_duck() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", DUCK_HEIGHT, 0.16)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func perform_duckduck_to_duck() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", DUCK_HEIGHT, 0.08)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func _enter(prev_state_id: int) -> void:
	match prev_state_id:
		STATES.UPRIGHT:
			perform_upright_to_duck()
		STATES.DUCKDUCK:
			perform_duckduck_to_duck()


func _physics_process(delta: float) -> void:
	if not Input.is_action_pressed("crouch"):
		switch_to(STATES.UPRIGHT)
	elif state_machine.movement.state_id in [MovementState.STATES.IDLE]:
		switch_to(STATES.DUCKDUCK)
