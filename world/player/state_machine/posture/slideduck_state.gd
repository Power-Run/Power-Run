class_name SlideDuckState
extends PostureState


var SLIDEDUCK_HEIGHT : float = 0.4


func perform_slideduck() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", SLIDEDUCK_HEIGHT, 0.5)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func _enter(prev_state_id: int) -> void:
	perform_slideduck()


func _physics_process(delta: float) -> void:
	if state_machine.movement.state_id in [MovementState.STATES.SLIDING]:
		switch_to(STATES.SLIDEDUCK)
