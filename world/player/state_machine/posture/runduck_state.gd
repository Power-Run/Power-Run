class_name RunDuckState
extends PostureState


var RUNDUCK_HEIGHT : float = 1.8 - 0.1


func perform_runduck() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", RUNDUCK_HEIGHT, 0.08)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func _enter(prev_state_id: int) -> void:
	perform_runduck()


func _physics_process(delta: float) -> void:
	if not Input.is_action_pressed("forward"):
		switch_to(STATES.UPRIGHT)
	elif state_machine.movement.state_id in [MovementState.STATES.CROUCHING]:
		switch_to(STATES.DUCKDUCK)
