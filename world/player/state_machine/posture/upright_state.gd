class_name UprightState
extends PostureState


func perform_upright() -> void:
	var tween : Tween = player.create_tween()
	tween.tween_property(player, "height", player.TARGET_HEIGHT, 0.16)
	tween.set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)


func _enter(prev_state_id: int) -> void:
	perform_upright()


# TODO eventually move to _input for performance?
#func _input(event: InputEvent) -> void:
func _physics_process(delta: float) -> void:
	if state_machine.movement.state_id in [MovementState.STATES.CROUCHING]:
		switch_to(STATES.DUCK)
	if state_machine.movement.state_id in [MovementState.STATES.SLIDING]:
		switch_to(STATES.DUCK)
	if Input.is_action_pressed("forward") and state_machine.movement.state_id in [MovementState.STATES.RUNNING]:
		switch_to(STATES.RUNDUCK)
