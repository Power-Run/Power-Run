class_name BaseState
extends Node


var state_machine : StateMachine
var player : Player
var state_id : int


func _init(state_machine : StateMachine) -> void:
	self.state_machine = state_machine
	self.player = state_machine.player


# manage switching from state to state
func switch_to(state_id : int) -> void:
	var type_klass = state_machine._type_klass_dict[self.type]
	var state_klass_dict : Dictionary = type_klass.state_klass_dict
	var next : BaseState = state_klass_dict[state_id].new(state_machine)
	next.state_id = state_id
	next.name = type_klass.STATES.keys()[state_id]
	if next.has_method("_pre_enter"):
		next.call("_pre_enter", self.state_id)
	if self.has_method("_exit"):
		self.call("_exit", next.state_id)
	state_machine.get(next.type).replace_by(next)
	state_machine.get(next.type).queue_free()
	state_machine.set(next.type, next)
	if next.has_method("_enter"):
		next.call("_enter", self.state_id)
	if self.has_method("_post_exit"):
		self.call("_post_exit", next.state_id)
