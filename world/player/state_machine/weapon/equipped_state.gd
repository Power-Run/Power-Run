class_name EquippedState
extends WeaponState


func _enter(prev_state_id: int) -> void:
	player.weapon.draw()


# TODO: _unhandled_key_input seems to be better but does not work for mosue input?
#func _unhandled_key_input(event: InputEvent) -> void:
func _input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle weapon"):
		switch_to(STATES.UNEQUIPPED)
	if event.is_action_pressed("ads"):
		switch_to(STATES.ADS)


func _physics_process(delta: float) -> void:
	if state_machine.movement in [MovementState.STATES.FLOATING]:
		switch_to(STATES.UNEQUIPPED)
