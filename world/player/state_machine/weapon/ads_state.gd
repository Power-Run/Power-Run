class_name AdsState
extends WeaponState

var prev_state_id : int = 0


# increase fov
func tween_fov() -> void:
	var tween : Tween = player.camera.create_tween()
	tween.tween_property(player.camera, "fov", 20, 0.06)
	tween.set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN)


# decrease fov
func tween_unfov() -> void:
	var tween : Tween = player.camera.create_tween()
	tween.tween_property(player.camera, "fov", 100, 0.08)
	tween.set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN)


func _enter(prev_state_id: int) -> void:
	self.prev_state_id = prev_state_id
	tween_fov()
	player.mouse_sensitivity = 0.025


func _exit(next_state_id: int) -> void:
	tween_unfov()
	player.mouse_sensitivity = 0.05


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ads") or event.is_action_released("ads"):
		switch_to(self.prev_state_id)
