class_name UnequippedState
extends WeaponState


func _enter(prev_state_id: int) -> void:
	player.weapon.holster()


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle weapon"):
		#get_viewport().set_input_as_handled()
		#if state_machine.movement.state_id not in [MovementState.STATES.FLOATING]:
		switch_to(STATES.EQUIPPED)
	if event.is_action_pressed("ads"):
		#if state_machine.movement.state_id not in [MovementState.STATES.FLOATING]:
		switch_to(STATES.ADS)
