class_name PostureState
extends BaseState

static var type : String = "posture"

enum STATES {NULL = 0, UPRIGHT, DUCK, DUCKDUCK, RUNDUCK, SLIDEDUCK}

static var state_klass_dict : Dictionary = {
	STATES.NULL : null,
	STATES.UPRIGHT : UprightState,
	STATES.DUCK : DuckState,
	STATES.RUNDUCK : RunDuckState,
	STATES.SLIDEDUCK : SlideDuckState,
	STATES.DUCKDUCK : DuckDuckState
}
