class_name WeaponState
extends BaseState

static var type : String = "weapon"

enum STATES {NULL = 0, ADS, EQUIPPED, UNEQUIPPED}

static var state_klass_dict : Dictionary = {
	STATES.NULL : null,
	STATES.ADS : AdsState,
	STATES.EQUIPPED : EquippedState,
	STATES.UNEQUIPPED : UnequippedState
}
