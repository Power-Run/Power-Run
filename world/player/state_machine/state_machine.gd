class_name StateMachine
extends Node

var player : Player

# holds all custom state klasses
static var type_klasses : Array = [
	MovementState,
	WeaponState,
	PostureState,
	CameraState
]

var _types : Dictionary = {}
var _type_klass_dict : Dictionary = {}


func _get(property: StringName) -> Variant:
	if property in _types:
		return _types[property]
	else:
		return null


func _get_property_list() -> Array[Dictionary]:
	var properties = []
	for property in _types:
		properties.append({"name": property, "type": TYPE_OBJECT})
	return properties


func _set(property: StringName, value: Variant) -> bool:
	if property in _type_klass_dict:
		_types[property] = value
		return true
	return false


func _init(player : Player, initial_states : Dictionary) -> void:
	self.name = "StateMachine"
	self.player = player
	player.add_child(self)
	for type_klass in type_klasses:
		var type : String = type_klass.type
		_type_klass_dict[type] = type_klass
		#var initial_state_klass = 
		var initial_state_id : int = initial_states[type] 
		var initial_state : BaseState = type_klass.state_klass_dict[initial_state_id].new(self)
		initial_state.state_id = initial_state_id
		initial_state.name = type_klass.STATES.keys()[initial_state_id]
		self.set(type, initial_state)
	for type_klass in type_klasses:
		var type : String = type_klass.type
		var initial_state : BaseState = self.get(type)
		#if initial_state.has_method("_pre_enter"):
			#initial_state.call("_pre_enter", type_klass.STATES.NULL)
		add_child(initial_state)
		#if initial_state.has_method("_enter"):
			#initial_state.call("_enter", type_klass.STATES.NULL)
