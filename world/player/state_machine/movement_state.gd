class_name MovementState
extends BaseState

static var type : String = "movement"

enum STATES {NULL = 0, IDLE, CROUCHING, FALLING, FLOATING, JUMPING, LANDING, RUNNING, SLIDING, STOPPAGE, WALKING, LANDINGSTUNN}

static var state_klass_dict : Dictionary = {
	STATES.NULL : null,
	STATES.IDLE : IdleState,
	STATES.CROUCHING : CrouchingState,
	STATES.FALLING : FallingState,
	STATES.FLOATING : FloatingState,
	STATES.JUMPING : JumpingState,
	STATES.LANDING : LandingState,
	STATES.RUNNING : RunningState,
	STATES.SLIDING : SlidingState,
	STATES.STOPPAGE : StoppageState,
	STATES.WALKING : WalkingState,
	STATES.LANDINGSTUNN : LandingStunnState
}
