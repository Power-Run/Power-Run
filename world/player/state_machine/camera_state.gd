class_name CameraState
extends BaseState

static var type : String = "camera"

enum STATES {NULL = 0, BREATHING, BOBBING}
#… LANDING, LIFTING, VIBRATING, HITPUNCH…

static var state_klass_dict : Dictionary = {
	STATES.NULL : null,
	STATES.BREATHING : BreathingState,
	STATES.BOBBING : BobbingState
}
