class_name BobbingState
extends CameraState

var BREATH_ANGLE_YAW : float = 0.0018
var BREATH_ANGLE_PITCH : float = 0.0005

var tween_yaw : Tween
var tween_pitch : Tween


func startup_bobbing_tweens():
	# TODO after LONG idle time look a bit away briefly
	# make breathing tween
	# TODO: randomize?? randf_range(1.6, 2)
	
	# tween yaw
	tween_yaw.tween_property(player.head, "bobbing_yaw", BREATH_ANGLE_YAW, 0.3).as_relative()
	tween_yaw.set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	tween_yaw.tween_property(player.head, "bobbing_yaw", -BREATH_ANGLE_YAW, 0.7).as_relative()
	tween_yaw.set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	
	# tween pitch 
	tween_pitch.tween_property(player.head, "bobbing_pitch", BREATH_ANGLE_PITCH, 0.3).as_relative()
	tween_pitch.set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	tween_pitch.tween_property(player.head, "bobbing_pitch", -2*BREATH_ANGLE_PITCH, 0.3).as_relative()
	tween_pitch.set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	tween_pitch.tween_property(player.head, "bobbing_pitch", BREATH_ANGLE_PITCH, 0.3).as_relative()
	tween_pitch.set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)

func _enter(prev_state_id: int) -> void:
	tween_yaw.play()
	tween_pitch.play()


func _exit(next_state_id: int) -> void:
	tween_yaw.stop()
	tween_pitch.stop()
	#player.head.create_tween().tween_property(player.head, "breathing_yaw", 0, 0.5).as_relative()
	#player.head.create_tween().tween_property(player.head, "breathing_pitch", 0, 0.5).as_relative()


func _init(state_machine : StateMachine) -> void:
	super(state_machine)
	tween_yaw = player.head.create_tween().set_loops()
	tween_pitch = player.head.create_tween().set_loops()
	startup_bobbing_tweens()


func _physics_process(delta: float) -> void:
	if state_machine.movement.state_id not in [MovementState.STATES.RUNNING]:
		switch_to(STATES.BREATHING)
