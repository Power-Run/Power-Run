class_name LandingState
extends MovementState

var YAW_ANGLE_FACTOR : float = 0.6
var FOV_THRESHOLD : float = 0.25
var FOV_FACTOR : float = 25


# yaw up/down on impact
func tween_bobbing(speed : float):
	# FIXME bobbing, breathing shaking needs not to override mouse input!
	var tween : Tween = player.head.create_tween()
	var angle : float = YAW_ANGLE_FACTOR * speed**1.4
	tween.tween_property(player.head, "landing_yaw", -angle, 0.07).as_relative()
	tween.set_trans(Tween.TRANS_BACK)
	tween.set_ease(Tween.EASE_OUT)
	tween.tween_property(player.head, "landing_yaw", angle * randf_range(0.8, 1.1), 0.7).as_relative()
	tween.set_trans(Tween.TRANS_BACK)
	tween.set_ease(Tween.EASE_IN)


# zoom fov on impact
func tween_fov(speed : float):
	var tween : Tween = player.camera.create_tween()
	var delta_fov : float = FOV_FACTOR * speed
	tween.tween_property(player.camera, "fov", -delta_fov, 0.01).as_relative()
	tween.set_trans(Tween.TRANS_CUBIC)
	tween.set_ease(Tween.EASE_OUT)
	tween.tween_property(player.camera, "fov", delta_fov, 0.3).as_relative()
	tween.set_trans(Tween.TRANS_CUBIC)
	tween.set_ease(Tween.EASE_OUT)


# calculate force of impact
func _impact_speed() -> float:
	for i in range(player.get_slide_collision_count()):
		var collision : KinematicCollision3D = player.get_slide_collision(i)
		for j in range(collision.get_collision_count()):
			# check if on floor
			if collision.get_normal(j).is_equal_approx(player.up_direction):
				var movement : Vector3 = collision.get_travel() + collision.get_remainder()
				return movement.project(player.up_direction).length()
	return 0.0


func _enter(prev_state_id: int) -> void:
	var speed : float = _impact_speed() # determine impact speed
	if not is_zero_approx(speed):
		player._last_impact_speed = speed

	tween_bobbing(speed) # trigger bobbing on impact
	if speed > 0.2 :
		switch_to(STATES.LANDINGSTUNN)
	elif Input.is_action_pressed("jump"): # allow instant jumping
		switch_to(STATES.JUMPING)
	elif speed > FOV_THRESHOLD:
		tween_fov(speed)
	elif player.hspeed() > 7:
		switch_to(STATES.SLIDING)
	elif player.hspeed() > 5:
		switch_to(STATES.RUNNING)
	elif player.hspeed() > 1:
		switch_to(STATES.WALKING)
	else:
		switch_to(STATES.STOPPAGE)
