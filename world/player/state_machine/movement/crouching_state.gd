class_name CrouchingState
extends MovementState

var ACCELERATION : float = 15
var FRICTION : float = 0.1
var CROUCHING_THRESHOLD : float = 3.8


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow floating
		switch_to(STATES.FLOATING)
	elif not player.is_on_floor(): # allow falling
		switch_to(STATES.FALLING)
	elif Input.is_action_just_pressed("jump"): # allow jump
		switch_to(STATES.JUMPING)
	#elif input_vector.is_zero_approx(): # stop on no input
		#switch_to(STATES.STOPPAGE)


	elif state_machine.posture.state_id in [PostureState.STATES.UPRIGHT]:
		switch_to(STATES.WALKING)
	elif player.hspeed() > CROUCHING_THRESHOLD:
		switch_to(STATES.SLIDING)
	else:
		var movement_direction : Vector3 = (player.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
		player.velocity -= FRICTION * player.speed() * player.velocity
		player.velocity += ACCELERATION * delta * movement_direction
	
