class_name FallingState
extends MovementState

var ACCELERATION : float = 2.0
var FRICTION : float = 0.0005
var GRAVITY : float = 20
var SLIDE_THRESHOLD : float = 2


func _can_slide() -> bool:
	var speed : float = player.hspeed()
	return player.is_on_floor() and speed > SLIDE_THRESHOLD


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow floating
		switch_to(STATES.FLOATING)
	elif state_machine.posture.state_id in [PostureState.STATES.DUCK] and _can_slide(): # allow sliding
		switch_to(STATES.SLIDING)
	elif player.is_on_floor():
		switch_to(STATES.LANDING)
	else:
		var movement_direction : Vector3 = (player.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
		player.velocity += ACCELERATION * delta * movement_direction
		player.velocity += GRAVITY * delta * player.gravity_direction
		player.velocity -= FRICTION * player.velocity
