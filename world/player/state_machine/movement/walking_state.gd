class_name WalkingState
extends MovementState

var ACCELERATION : float = 86.65
var FRICTION : float = 0.1
var SLIDE_THRESHOLD : float = 3


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow floating
		switch_to(STATES.FLOATING)
	elif not player.is_on_floor(): # allow falling
		switch_to(STATES.FALLING)
	elif Input.is_action_just_pressed("jump"): # allow jump
		switch_to(STATES.JUMPING)
	#elif input_vector.is_zero_approx(): # stop on no input
		#switch_to(STATES.STOPPAGE)
	elif input_vector.is_zero_approx(): # stop without input
		switch_to(STATES.STOPPAGE)

	elif state_machine.posture.state_id in [PostureState.STATES.DUCK]:
		if player.speed() < SLIDE_THRESHOLD:
			switch_to(STATES.CROUCHING)
		else:
			switch_to(STATES.SLIDING)
			# FIXME: strafing does not release into running
	elif not Input.is_action_pressed("walk") and Input.is_action_pressed("forward") and not (Input.is_action_pressed("left") or Input.is_action_pressed("right") or Input.is_action_pressed("back")):
		switch_to(STATES.RUNNING)
	else:
		var movement_direction : Vector3 = (player.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
		player.velocity -= FRICTION * player.speed() * player.velocity
		player.velocity += ACCELERATION * delta * movement_direction
