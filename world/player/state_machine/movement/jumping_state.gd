class_name JumpingState
extends MovementState
@onready var upright_ray_cast: RayCast3D = $"../../UprightRayCast"

var JUMPING_ANGLE : float = 0.1
var JUMP_FORCE : Dictionary = {
	STATES.CROUCHING: 13.0,
	STATES.FALLING: 0,
	STATES.FLOATING: 0,
	STATES.IDLE: 13.0,
	STATES.JUMPING: 0,
	STATES.LANDING: 7,
	STATES.RUNNING: 7.0,
	STATES.SLIDING: 13.0,
	STATES.STOPPAGE: 0,
	STATES.WALKING: 7.0,
}


func perform_jump(force : float) -> void:
	var input_plane_normal : Vector3 = player.global_transform.basis * Vector3(0, 1, 0)
	player.velocity += force * input_plane_normal
	


# bobbin on jump start
func tween_jumping():
	var tween : Tween = player.head.create_tween()
	tween.tween_property(player.head, "jumping_yaw", -JUMPING_ANGLE, 0.12)
	tween.set_trans(Tween.TRANS_CUBIC)
	tween.set_ease(Tween.EASE_OUT)
	tween.tween_property(player.head, "jumping_yaw", 0, 1.2)
	tween.set_trans(Tween.TRANS_ELASTIC)
	tween.set_ease(Tween.EASE_IN)


func _enter(prev_state_id: int) -> void:
	perform_jump(JUMP_FORCE[prev_state_id])
	tween_jumping()


func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed("ghost"): # allow instant floating
		switch_to(STATES.FLOATING)
	switch_to(STATES.FALLING)
	#if not player.is_on_floor():
