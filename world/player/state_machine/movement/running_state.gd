class_name RunningState
extends MovementState

var ACCELERATION : float = 150
var FRICTION : float = 0.1
var HOLSTERED_ACCELERATION : float = 194.95
var SLIDE_THRESHOLD : float = 2


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow instant floating
		switch_to(STATES.FLOATING)
	elif Input.is_action_just_pressed("jump"): # allow jumping
		switch_to(STATES.JUMPING)
	elif not player.is_on_floor(): # allow falling
		switch_to(STATES.FALLING)
	#elif input_vector.is_zero_approx(): # stop on no input
		#switch_to(STATES.STOPPAGE)
	elif input_vector.is_zero_approx(): # stop without input
		switch_to(STATES.STOPPAGE)


	if Input.is_action_pressed("walk"):
		switch_to(STATES.WALKING)
	if Input.is_action_pressed("crouch"):
		if player.speed() < SLIDE_THRESHOLD:
			switch_to(STATES.CROUCHING)
		else:
			switch_to(STATES.SLIDING)
	elif not Input.is_action_pressed("forward"):
		switch_to(STATES.WALKING)
	else:
		var movement_direction : Vector3 = (player.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
		player.velocity -= FRICTION * player.speed() * player.velocity
		var accel : float = ACCELERATION if state_machine.weapon.state_id in [WeaponState.STATES.EQUIPPED] else HOLSTERED_ACCELERATION
		player.velocity += accel * delta * movement_direction
