class_name StoppageState
extends MovementState

var IMPLICIT_MAX_SPEED : float = 3.8
var DECELERATION : float = 0.80
var CUTOFF : float = 0.05
var SLIDE_THRESHOLD : float = 3.8



func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow instant floating
		switch_to(STATES.FLOATING)
	if player.speed() < CUTOFF:
		switch_to(STATES.IDLE)
	elif not input_vector.is_zero_approx() and player.hspeed() < 4:
		switch_to(STATES.WALKING)
	else:
		# FIXME
		player.velocity -= DECELERATION * player.velocity / sqrt(max(1, IMPLICIT_MAX_SPEED**2 - player.velocity.length_squared()))
