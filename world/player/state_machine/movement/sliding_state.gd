class_name SlidingState
extends MovementState

var ACCELERATION : float = 0.5
var FRICTION : float = 0.001
var SLIDE_THRESHOLD : float = 3.8


# increase fov
func tween_fov() -> void:
	var tween : Tween = player.camera.create_tween()
	tween.tween_property(player.camera, "fov", 110, 0.06)
	tween.set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN)


# decrease fov
func tween_unfov() -> void:
	var tween : Tween = player.camera.create_tween()
	tween.tween_property(player.camera, "fov", 100, 0.08)
	tween.set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN)


func _enter(prev_state_id: int) -> void:
	if not prev_state_id == STATES.SLIDING:
		tween_fov()


func _exit(next_state_id: int) -> void:
	if not next_state_id == STATES.CROUCHING and not next_state_id == STATES.FALLING:
		tween_unfov()


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow floating
		switch_to(STATES.FLOATING)
	elif not player.is_on_floor(): # allow falling
		switch_to(STATES.FALLING)
	elif Input.is_action_just_pressed("jump"): # allow jump
		switch_to(STATES.JUMPING)
	#elif input_vector.is_zero_approx(): # stop on no input
		#switch_to(STATES.STOPPAGE)


	elif player.hspeed() < SLIDE_THRESHOLD:
		switch_to(STATES.CROUCHING)
	elif state_machine.posture.state_id in [PostureState.STATES.UPRIGHT]:
		switch_to(STATES.WALKING)
	else:
		var movement_direction : Vector3 = (player.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
		player.velocity -= FRICTION * player.hspeed() * player.velocity
		player.velocity += ACCELERATION * delta * movement_direction
