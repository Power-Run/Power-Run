class_name FloatingState
extends MovementState

var ACCELERATION : float = 400
var FRICTION : float = 0.1


func tween_fov() -> void:
	var tween : Tween = player.camera.create_tween()
	tween.tween_property(player.camera, "fov", 15, 0.2).as_relative()
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_IN_OUT)


func tween_unfov() -> void:
	var tween : Tween = player.camera.create_tween()
	tween.tween_property(player.camera, "fov", -15, 0.2).as_relative()
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_IN_OUT)
	
	
func _enter(prev_state_id: int) -> void:
	tween_fov()
	#state_machine.weapon.switch_to(WeaponState.STATES.UNEQUIPPED)


func _exit(next_state_id: int) -> void:
	tween_unfov()
	#state_machine.weapon.switch_to(WeaponState.STATES.EQUIPPED)


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"):
		switch_to(STATES.FALLING)
	else:
		# FIXME: strafe up/down with jump/crouch
		var input_direction : Vector3 = player.camera.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)
		player.velocity -= FRICTION * player.velocity
		player.velocity += ACCELERATION * delta * input_direction
