class_name IdleState
extends MovementState


func _physics_process(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	if Input.is_action_just_pressed("ghost"): # allow floating
		switch_to(STATES.FLOATING)
	elif not player.is_on_floor(): # allow falling
		switch_to(STATES.FALLING)
	elif Input.is_action_just_pressed("jump"): # allow jump
		switch_to(STATES.JUMPING)
	#elif input_vector.is_zero_approx(): # stop on no input
		#switch_to(STATES.STOPPAGE)
	
	
	elif Input.is_action_just_pressed("crouch"): # allow crouching
		switch_to(STATES.CROUCHING)
	elif not input_vector.is_zero_approx() and state_machine.posture.state_id in [PostureState.STATES.UPRIGHT, PostureState.STATES.RUNDUCK]:
		switch_to(STATES.WALKING)
	else:
		player.velocity = Vector3.ZERO
