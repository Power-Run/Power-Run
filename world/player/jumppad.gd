class_name Jumppad
extends Area3D

# (optional) icon to show in the editor dialogs:
# @icon("res://path/to/optional/icon.svg")

@export var FORCE_VECTOR : Vector3
@export var TARGET_NORMAL : Vector3		# the plane, that will be switched to
@export var WAIT_TIME : float = 2		# time before switching begins
@export var SWITCH_TIME : float	= 1		# time, that switching will take

func _ready():
	set_monitoring(true)                            		# to emit signal
	self.body_entered.connect(_on_Jumppad_body_entered)		# connect signals

# TODO: Handle bodies other than CharacterBody3D
func _on_Jumppad_body_entered(body: Node3D):
	if not body.is_in_group("jumppad-enabled"):
		return
	# Let the body jump
	body.velocity -= self.FORCE_VECTOR			# only CharacterBody3D has velocity
	# Create and start plane switching event for body according to parameters
	PlaneSwitchEvent.new(body, self.TARGET_NORMAL, self.WAIT_TIME, self.SWITCH_TIME).start()
