extends MeshInstance3D


func holster() -> void:
	visible = false


func draw() -> void:
	visible = true
