class_name Player
extends CharacterBody3D

@export var INITIAL_ORIENTATION_VECTOR : Vector3 = Vector3.DOWN
@export var START_HEIGHT : float = 50.0
@export	var SLIDE_THRESHOLD : float = 2

@export var FOV_DEFAULT : float = 100.0
@export var MOUSE_SENSITIVITY_DEFAULT : float = 0.05
var mouse_sensitivity : float

@onready var standing_ray_cast: RayCast3D = $CollisionShapeBottom/StandingRayCast
@onready var focus: Marker3D = %Focus


@export var INVERT_MOUSE_DEFAULT : bool = false
var invert_mouse : int

@export var SWITCH_KEY_WAIT_TIME : float = 0.1
@export var SWITCH_KEY_SWITCH_TIME : float = 1.5

@onready var head: Node3D = %head
@onready var neck: Node3D = %neck
@onready var camera: Camera3D = %Camera

@onready var weapon: MeshInstance3D = %weapon

@onready var _top : CollisionShape3D = $CollisionShapeTop

@onready var _bottom : CollisionShape3D = $CollisionShapeBottom

@onready var event_action_dict : Dictionary = build_event_action_dict()
@onready var current_orientation : Orientation = Global.orientations.get_orientation_from_vector(INITIAL_ORIENTATION_VECTOR)
@onready var gravity_direction = current_orientation.vector

@onready var hitbox_anim_tree: AnimationTree = %HitboxAnimTree
@onready var hitbox_anim: AnimationPlayer = %HitboxAnim
@onready var bob_anim_tree: AnimationTree = %BobAnimTree
@onready var bob_anim: AnimationPlayer = %BobAnim

@onready var state_chart : StateChart = $StateChart
@onready var state_chart_debugger: MarginContainer = $StateChartDebugger

var debug : bool = false

var _last_impact_speed : float = 0.0


func build_event_action_dict():
	var d : Dictionary = {}
	for action in InputMap.get_actions():
		var events = InputMap.action_get_events(action)
		for event in events:
			if event is InputEventKey:
				d[event.keycode] = String(action)
	return d


# player speed
func speed() -> float:
	return self.velocity.length()


# horizontal player speed
func hspeed() -> float:
	return self.velocity.cross(self.up_direction).length()


# vertical player speed
func vspeed() -> float:
	return -self.velocity.dot(self.up_direction)


# set player fow tween
# TODO: accept tween ease and type
func tween_fov(fov: float, time: float) -> void:
	var tween : Tween = self.camera.create_tween()
	tween.tween_property(self.camera, "fov", fov, time)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)


# impact speed
func _impact_speed() -> float:
	for i in range(self.get_slide_collision_count()):
		var collision : KinematicCollision3D = self.get_slide_collision(i)
		for j in range(collision.get_collision_count()):
			# check if on floor
			if collision.get_normal(j).is_equal_approx(self.up_direction):
				var movement : Vector3 = collision.get_travel() + collision.get_remainder()
				return movement.project(self.up_direction).length()
	return 0.0


func _ready():
	# Capture mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	# not using jump-pads
	#self.add_to_group("jumppad-enabled")
	
	# load settings
	var settings : ConfigFile = $"/root/Game".settings
	self.mouse_sensitivity = settings.get_value("GUI", "mouse_sensitivity", MOUSE_SENSITIVITY_DEFAULT)
	self.invert_mouse = -1 if settings.get_value("GUI", "invert_mouse", INVERT_MOUSE_DEFAULT) else 1

	# Inititalize StateChartProperties with defaults so that it can be used in StateCharts
	self.state_chart_debugger.enabled = false
	$StateChart.set_expression_property("can_stand", false)
	$StateChart.set_expression_property("is_onfloor", false)
	$StateChart.set_expression_property("is_onwall", false)
	
	#SPEEDS
	$StateChart.set_expression_property("speed", 0)
	$StateChart.set_expression_property("vspeed", 0)
	$StateChart.set_expression_property("hspeed", 0)
	$StateChart.set_expression_property("impact", 0)
	$StateChart.set_expression_property("last_impact", 0)
	
	#THRESHOLDS
	$StateChart.set_expression_property("slide_threshold", 3.8)
	$StateChart.set_expression_property("run_threshold", 5)
	$StateChart.set_expression_property("crouch_threshold", 2)
	$StateChart.set_expression_property("stand_threshold", 1)
	
	#KEYS
	$StateChart.set_expression_property("forward_just_pressed", false)
	$StateChart.set_expression_property("forward_pressed", false)
	$StateChart.set_expression_property("back_just_pressed", false)
	$StateChart.set_expression_property("back_pressed", false)
	$StateChart.set_expression_property("left_just_pressed", false)
	$StateChart.set_expression_property("left_pressed", false)
	$StateChart.set_expression_property("right_just_pressed", false)
	$StateChart.set_expression_property("right_pressed", false)
	$StateChart.set_expression_property("crouch_just_pressed", false)
	$StateChart.set_expression_property("crouch_pressed", false)
	$StateChart.set_expression_property("walk_just_pressed", false)
	$StateChart.set_expression_property("walk_pressed", false)
	$StateChart.set_expression_property("jump_just_pressed", false)
	$StateChart.set_expression_property("jump_pressed", false)
	$StateChart.set_expression_property("ghost_just_pressed", false)
	$StateChart.set_expression_property("ghost_pressed", false)


func _exit_tree() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)


func _input(event: InputEvent) -> void:

	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		#rotate camera view left/right
		rotate(self.gravity_direction, deg_to_rad(event.relative.x*mouse_sensitivity))
		#rotate camera view up/down
		head.rotation.x += deg_to_rad(-event.relative.y*mouse_sensitivity*invert_mouse)
	if event is InputEventKey and event.keycode in event_action_dict:
		var action_name = event_action_dict[event.keycode]
		match action_name:
			"plane_closest":
				if event.pressed:
					# find out closest plane
					var forward : Vector3 = self.camera.get_global_transform().basis.z.normalized()
					var closest_orientation : Orientation = current_orientation
					var smallest_distance : float = forward.distance_squared_to(-current_orientation.vector)
					for orientation in Global.orientations.orientations:
						var d : float = forward.distance_squared_to(-orientation.vector)
						if d < smallest_distance:
							smallest_distance = d
							closest_orientation = orientation
					if closest_orientation != current_orientation:
						# Switch to another plane
						print("switching to ", closest_orientation.name)
						PlaneSwitchEvent.new(self, closest_orientation.vector, SWITCH_KEY_WAIT_TIME, SWITCH_KEY_SWITCH_TIME).start()
						current_orientation = closest_orientation
			"plane1", "plane2", "plane3", "plane4", "plane5", "plane6":
				if event.pressed:
					var new_orientation : Orientation = Global.orientations.get_orientation_from_name(action_name)
					if new_orientation != current_orientation:
						# Switch to another plane
						print("switching to ", action_name)
						PlaneSwitchEvent.new(self, new_orientation.vector, SWITCH_KEY_WAIT_TIME, SWITCH_KEY_SWITCH_TIME).start()
						current_orientation = new_orientation
			"ui_cancel":
				if event.pressed:
					# Capturing/Freeing the cursor
					Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE else Input.MOUSE_MODE_VISIBLE)


	if is_on_wall():
		print("ONWALL!")
	else:
		print("NOT ONWALL!")
		
	# Toggle StateCharDebugger scene on/off
	if Input.is_action_just_pressed("debug"):
		if self.state_chart_debugger.enabled:
			self.state_chart_debugger.enabled = false
		else:
			self.state_chart_debugger.enabled = true


func _physics_process(delta: float) -> void:
	# set 2D blend position in animation
	bob_anim_tree.set("parameters/blend_position", Vector2(self.velocity.x, self.velocity.z))
	print(self.velocity.normalized().x, self.velocity.normalized().z)
		
	move_and_slide()
	
	#scale speed of bob_run animation according to player speed
	bob_anim_tree.set('parameters/RunTimeScale/scale', hspeed() * 0.6)
	
	# stabilize camera by looking at Focus object
	#camera.look_at(focus.global_transform.origin, -gravity_direction)
	#head.look_at(focus.global_transform.origin, up_direction)
	# FIXME this needs to be uncommented for camera looking at focus
	neck.look_at(focus.global_transform.origin, self.get_global_transform().basis.y)

	# FIXME: prevent hyper speed bug workaround
	if self.speed() > 200:
		self.velocity = Vector3.ZERO
	
	##### Calculate StateChart properties -> remember to initialize in _ready()
	
	# set StateChart is_onfloor state
	$StateChart.set_expression_property("is_onfloor", self.is_on_floor())
	# set StateChart is_onwall state
	$StateChart.set_expression_property("is_onwall", self.is_on_wall())
	# set StateChart StandingRaycast info
	$StateChart.set_expression_property("can_stand", !standing_ray_cast.is_colliding())
	# set StateChart speeds
	$StateChart.set_expression_property("speed", speed())
	$StateChart.set_expression_property("hspeed", hspeed())
	$StateChart.set_expression_property("vspeed", vspeed())
	# set StateChart impact
	$StateChart.set_expression_property("impcat", _impact_speed())
	$StateChart.set_expression_property("last_impcat", _last_impact_speed)
	# set StateChart forward/back/left/right button state
	$StateChart.set_expression_property("forward_just_pressed", Input.is_action_just_pressed("forward"))
	$StateChart.set_expression_property("forward_pressed", Input.is_action_pressed("forward"))
	$StateChart.set_expression_property("back_just_pressed", Input.is_action_just_pressed("back"))
	$StateChart.set_expression_property("back_pressed", Input.is_action_pressed("back"))
	$StateChart.set_expression_property("left_just_pressed", Input.is_action_just_pressed("left"))
	$StateChart.set_expression_property("left_pressed", Input.is_action_pressed("left"))
	$StateChart.set_expression_property("right_just_pressed", Input.is_action_just_pressed("right"))
	$StateChart.set_expression_property("right_pressed", Input.is_action_pressed("right"))
	# set StateChart crouch button state
	$StateChart.set_expression_property("crouch_just_pressed", Input.is_action_just_pressed("crouch"))
	$StateChart.set_expression_property("crouch_pressed", Input.is_action_pressed("crouch"))
	# set StateChart walk button state
	$StateChart.set_expression_property("walk_just_pressed", Input.is_action_just_pressed("walk"))
	$StateChart.set_expression_property("walk_pressed", Input.is_action_pressed("walk"))
	# set StateChart jump button state
	$StateChart.set_expression_property("jump_just_pressed", Input.is_action_just_pressed("jump"))
	$StateChart.set_expression_property("jump_pressed", Input.is_action_pressed("jump"))
	# set StateChart ghost button state
	$StateChart.set_expression_property("ghost_just_pressed", Input.is_action_just_pressed("ghost"))
	$StateChart.set_expression_property("ghost_pressed", Input.is_action_pressed("ghost"))


###### MOVEMENT ####


# Grounded
func _on_grounded_state_entered() -> void:
	# blend from aircrouch to crouch
	# hitbox_anim_tree.set("parameters/Blend_crouch_aircrouch/blend_amount", 0)
	var tween_aircrouch : Tween = self.create_tween()
	tween_aircrouch.tween_property(hitbox_anim_tree, "parameters/Blend_crouch_aircrouch/blend_amount", 0, 0.1)
	tween_aircrouch.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	# determine impact speed
	var speed : float = _impact_speed()
	if not is_zero_approx(speed):
		self._last_impact_speed = speed

# Airborne
func _on_airborne_state_entered() -> void:
	# blend from crouch to aircrouch
	# hitbox_anim_tree.set("parameters/Blend_crouch_aircrouch/blend_amount", 1)
	var tween_aircrouch : Tween = self.create_tween()
	tween_aircrouch.tween_property(hitbox_anim_tree, "parameters/Blend_crouch_aircrouch/blend_amount", 1, 0.1)
	tween_aircrouch.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)


# Jumping
func _on_jumping_state_entered() -> void:
	# jumping-movement
	var input_plane_normal : Vector3 = self.global_transform.basis * Vector3(0, 1, 0)
	self.velocity += 6 * input_plane_normal



# Air-Jumping
func _on_air_jumping_state_entered() -> void:
	# air-jumping-movement
	var input_plane_normal : Vector3 = self.global_transform.basis * Vector3(0, 1, -0.2)
	# add randomness to jump height 
	#self.velocity += randf_range(6, 8) * input_plane_normal
	self.velocity += 8 * input_plane_normal


# Ghosting
func _on_ghosting_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.0, 0.1)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	
	# set FOV
	tween_fov(FOV_DEFAULT +25, 0.2)

func _on_ghosting_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 180
	var FRICTION : float = 0.2
	
	# ghosting-movement
	var input_direction : Vector3 = self.camera.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)
	self.velocity -= FRICTION * self.velocity/2
	self.velocity += ACCELERATION * delta * input_direction


func _on_ghosting_state_exited() -> void:
	# un-set FOV
	tween_fov(FOV_DEFAULT, 0.2)


# Falling
func _on_falling_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 1, 0.1)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)


func _on_falling_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 2.0
	var FRICTION : float = 0.0008
	var GRAVITY : float = 20

	# falling-movement
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity += ACCELERATION * delta * movement_direction
	self.velocity += GRAVITY * delta * self.gravity_direction
	self.velocity -= FRICTION * self.velocity


# Running
func _on_running_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.85, 0.1)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	
	## set bob
	bob_anim.play("bob_run")
	bob_anim.set('speed_scale', 3)


func _on_running_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 194.95
	var FRICTION : float = 0.1
	#var HOLSTERED_ACCELERATION : float = 194.95
	#var WEAPON_ACCELERATION : float = 150

	# running-movement
	self.velocity -= FRICTION * self.speed() * self.velocity
	var accel : float = ACCELERATION
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity += accel * delta * movement_direction


# Walking
func _on_walking_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.9, 0.1)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)

	## set bob
	#bob_anim.play("bob_walk")

	#bob_anim.set('speed_scale', 2.5)
	# set TEST bob
	#bob_anim.play("bob_walk")
	#bob_anim.set('speed_scale', 1)
	#bob_anim_tree.set("parameters/WalkSpace2D/Active", true)


func _on_walking_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 86.55
	var FRICTION : float = 0.1
	
	# walking-movement
	self.velocity -= FRICTION * self.speed() * self.velocity
	var accel : float = ACCELERATION
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity += accel * delta * movement_direction
	


# Crouching
func _on_crouching_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.27, 0.1)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)

	# set bob
	bob_anim.play("bob_crouch")
	bob_anim.set('speed_scale', 1.5)

func _on_crouching_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 15
	var FRICTION : float = 0.1
	
	# crouching-movement
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity -= FRICTION * self.speed() * self.velocity
	self.velocity += ACCELERATION * delta * movement_direction


# Aircrouching
func _on_aircrouch_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.27, 0.1)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)


func _on_aircrouch_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 2.0
	var FRICTION : float = 0.008
	var GRAVITY : float = 20

	# aircrouching-movement
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity += ACCELERATION * delta * movement_direction
	self.velocity += GRAVITY * delta * self.gravity_direction
	self.velocity -= FRICTION * self.velocity


# Sliding
func _on_sliding_state_entered() -> void:
	# change hitbox
	var hitbox_tween : Tween = self.create_tween()
	hitbox_tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.2, 0.05)
	hitbox_tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)
	
	# set bob
	bob_anim.play("bob_slide")
	bob_anim.set('speed_scale', 1)
	
	# set FOV
	tween_fov(FOV_DEFAULT + 15, 0.1)

func _on_sliding_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 0.5
	var FRICTION : float = 0.001

	# sliding-movement
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity -= FRICTION * self.hspeed() * self.velocity
	self.velocity += ACCELERATION * delta * movement_direction


func _on_sliding_state_exited() -> void:
	# un-set FOV
	tween_fov(FOV_DEFAULT, 0.5)


# Sitting
func _on_sitting_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 0.2, 0.1)
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)

	# set bob
	bob_anim.play("bob_breathe")
	bob_anim.set('speed_scale', 0.5)

func _on_sitting_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var IMPLICIT_MAX_SPEED : float = 3.8
	var DECELERATION : float = 0.80
	
	# sitting-movement
	self.velocity -= DECELERATION * self.velocity / sqrt(max(1, IMPLICIT_MAX_SPEED**2 - self.velocity.length_squared()))
	if self.speed() < 0.05:
		self.velocity = Vector3.ZERO

# Standing
func _on_standing_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 1.0, 0.2)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)

	# set bob
	#bob_anim.play("bob_breathe")
	#bob_anim.set('speed_scale', 0.5)
	#bob_anim.play("bob_walk_strafe_right")
	#bob_anim.set('speed_scale', 1)


func _on_standing_state_physics_processing(delta: float) -> void:
	var IMPLICIT_MAX_SPEED : float = 3.8
	var DECELERATION : float = 0.80


	# standing-movement
	self.velocity -= DECELERATION * self.velocity / sqrt(max(1, IMPLICIT_MAX_SPEED**2 - self.velocity.length_squared()))
	if self.speed() < 0.05:
		self.velocity = Vector3.ZERO

# Climbing
func _on_climbing_state_entered() -> void:
	# set hitbox
	var tween : Tween = self.create_tween()
	tween.tween_property(hitbox_anim_tree, "parameters/Blend_stand_crouch/blend_amount", 1.0, 0.7)
	tween.set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)


func _on_climbing_state_physics_processing(delta: float) -> void:
	var input_vector : Vector2 = Input.get_vector("right", "left", "back", "forward")
	var ACCELERATION : float = 15
	var FRICTION : float = 0.1
	var GRAVITY : float = 2
	
	# climbing-movement
	var movement_direction : Vector3 = (self.global_transform.basis * Vector3(-input_vector.x, 0, -input_vector.y)).normalized()
	self.velocity -= FRICTION * self.speed() * self.velocity
	self.velocity += ACCELERATION * delta * movement_direction * self.gravity_direction
	self.velocity += GRAVITY * delta * self.gravity_direction


# WEAPON ####
# holster
func _on_holstered_state_entered() -> void:
	weapon.holster()


func _on_holstered_state_input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("toggle weapon"):
		state_chart.send_event("draw")


# draw
func _on_drawn_state_entered() -> void:
	weapon.draw()


func _on_drawn_state_input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("toggle weapon"):
		state_chart.send_event("holster")


# ADS


func _on_ads_state_entered() -> void:
	var cam_fov_tween : Tween = camera.create_tween()
	cam_fov_tween.tween_property(camera, "fov", 30, 0.06)
	cam_fov_tween.set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN)
	
	self.mouse_sensitivity = 0.025


func _on_ads_state_exited() -> void:
	var cam_fov_tween : Tween = camera.create_tween()
	cam_fov_tween.tween_property(camera, "fov", FOV_DEFAULT, 0.08)
	cam_fov_tween.set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN)
	
	self.mouse_sensitivity = MOUSE_SENSITIVITY_DEFAULT


func _on_ads_state_input(event: InputEvent) -> void:
	if not Input.is_action_pressed("ads"):
		state_chart.send_event("hipfire")


# Hipfire
func _on_hipfire_state_input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("ads"):
		state_chart.send_event("ADS")
