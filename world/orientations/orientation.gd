class_name Orientation
extends Object

var name : String
var vector : Vector3

func _init(name: String, vector: Vector3) -> void:
	self.name = name
	self.vector = vector
