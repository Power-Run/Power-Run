class_name Orientations
extends Object

var plane1 : Orientation = Orientation.new("plane1", Vector3.UP)
var plane2 : Orientation = Orientation.new("plane2", Vector3.FORWARD)
var plane3 : Orientation = Orientation.new("plane3", Vector3.RIGHT)
var plane4 : Orientation = Orientation.new("plane4", Vector3.LEFT)
var plane5 : Orientation = Orientation.new("plane5", Vector3.BACK)
var plane6 : Orientation = Orientation.new("plane6", Vector3.DOWN)
#var orientations = [plane1, plane2, plane3, plane4, plane5, plane6]
# clamp to only three planes
var orientations = [plane2, plane4, plane6]

func get_orientation_from_name(name: String) -> Orientation:
	var orientations: Dictionary = {
		"plane1": plane1,
		"plane2": plane2,
		"plane3": plane3,
		"plane4": plane4,
		"plane5": plane5,
		"plane6": plane6
	}
	return orientations[name]


func get_orientation_from_vector(vector: Vector3) -> Orientation:
	var orientations: Dictionary = {
		Vector3.UP: plane1,
		Vector3.FORWARD: plane2,
		Vector3.RIGHT: plane3,
		Vector3.LEFT: plane4,
		Vector3.BACK: plane5,
		Vector3.DOWN: plane6
	}
	return orientations[vector]
