class_name Level
extends Node3D


const GAP_OFFSET_A : int = 0
const GAP_OFFSET_B : int = 0
#const GAP_OFFSET_B : int = 512

var settings : ConfigFile
var seed : String
var selected_biome_names : Array = []
var blocks_per_side_xz  : int
var blocks_per_side_yx  : int
var blocks_per_side_zy  : int

var rng : RandomNumberGenerator = RandomNumberGenerator.new()
var jitter_rng : RandomNumberGenerator = RandomNumberGenerator.new()
var config : ConfigFile = ConfigFile.new()
var seen_biomes : Dictionary = {}
var building_names : Array = []
var start_block_coordinate : Vector2
var side1 : Side
var side2 : Side
var side3 : Side


var polyomino_configuration : PolyominoConfiguration

class PolyominoConfiguration:
	var xz_polyomino : Array
	var yx_polyomino : Array
	var zy_polyomino : Array
	var weight : float
	func _init(dict : Dictionary):
		self.xz_polyomino = dict["xz"]
		self.yx_polyomino = dict["yx"]
		self.zy_polyomino = dict["zy"]
		self.weight = dict.get("w", 1)


func get_random_biome() -> Biome:
	var names : Array = self.selected_biome_names
	var biome_name : String = names[self.rng.randi_range(0, len(names) - 1)]
	if not biome_name in seen_biomes:
		seen_biomes[biome_name] = Biome.new(biome_name)
	return seen_biomes[biome_name]


func get_random_building_name() -> String:
	var names : Array = self.building_names
	return names[self.rng.randi_range(0, len(names) - 1)]


func get_random_mask():
	var masks : Array = self.config.get_value("mondrian_masks", "masks")
	return masks[self.rng.randi_range(0, len(masks) - 1)]


func get_random_tile_raise():
	var tile_raises : Array = self.config.get_value("block", "tile_raises")
	return tile_raises[self.rng.randi_range(0, len(tile_raises) - 1)]


func _load_settings() -> void:
	seed = settings.get_value("GUI", "level_seed")
	selected_biome_names = settings.get_value("GUI", "level_selected_biome_names")
	blocks_per_side_xz = settings.get_value("GUI", "level_blocks_per_side_xz")
	blocks_per_side_yx = settings.get_value("GUI", "level_blocks_per_side_yx")
	blocks_per_side_zy = settings.get_value("GUI", "level_blocks_per_side_zy")


func _init():
	var scene_tree : SceneTree = Engine.get_main_loop()
	self.settings = scene_tree.root.get_node("/root/Game").settings
	self._load_settings()
	# set rng seed; hash produces 32bit int
	self.rng.seed = hash(self.seed)
	self.jitter_rng.seed = hash(self.seed)
	# read config
	self.config.load("res://world/level.cfg")
	# read biomes and buildings
	self.building_names = Helper.get_subdirectory_list("res://world/buildings")
	# uncomment next line to only get listed buildings
	#self.building_names = ["Quartex"] # get_subdirectory_list("res://world/buildings")
	config.set_value( "video", "vsync", false)
	print("vsync disabled")
	
	self.config.load("res://world/polyominoes.cfg")
	var polyominoes = config.get_value("polyominoes", "polyominoes")
	self.polyomino_configuration = PolyominoConfiguration.new(polyominoes[self.rng.randi_range(0, len(polyominoes) - 1)])
	
	# initialize progress bar
	Global.load_block.emit(self.blocks_per_side_xz + self.blocks_per_side_yx + self.blocks_per_side_zy)
	
	# add side xz
	var count : int = self.blocks_per_side_xz
	var coordinates : Array = self.polyomino_configuration.xz_polyomino.slice(0, count)
	self.start_block_coordinate = Vector2(0,0) if coordinates.is_empty() else coordinates[0] 
	var position : Vector3 = Vector3(GAP_OFFSET_A/2, 0, GAP_OFFSET_B/2)
	side1 = Side.new(self, coordinates, position)
	side1.name = "Side_xz"
	
	# add side yx
	count = self.blocks_per_side_yx
	coordinates = self.polyomino_configuration.yx_polyomino.slice(0, count)
	position = Vector3(GAP_OFFSET_B/2, GAP_OFFSET_A/2, 0)
	side2 = Side.new(self, coordinates, position)
	side2.name = "Side_yx"
	side2.rotation.y = -PI/2
	side2.rotation.x = -PI/2
	
	# add side zy
	count = self.blocks_per_side_zy
	coordinates = self.polyomino_configuration.zy_polyomino.slice(0, count)
	position = Vector3(0, GAP_OFFSET_B/2, GAP_OFFSET_A/2)
	side3 = Side.new(self, coordinates, position)
	side3.name = "Side_zy"
	side3.rotation.y = PI/2
	side3.rotation.z = PI/2


func _ready():
	add_child(side1)
	add_child(side2)
	add_child(side3)
	
	# replace placeholder with player
	var player : Player = %Player.create_instance(true)
	player.name = "Player"
	# make %Player accessible
	player.owner = $"/root"
	player.unique_name_in_owner = true
	var scale : float = 8*Block.FORM_SCALE
	player.position = Vector3(scale*(self.start_block_coordinate.x + 0.5), player.START_HEIGHT, scale*(self.start_block_coordinate.y + 0.5))
