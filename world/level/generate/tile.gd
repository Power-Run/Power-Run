class_name Tile
extends VirtualNode


var biome : Biome
const FORM_SCALE : int = 64


func _init(level : Level, side : Side, block : Block, _form_type : int, position : Vector3, global_position : Vector3):
	super(_form_type, FORM_SCALE, position, global_position)
	self.name = "Tile_(%d,%d)" % [block.form.x, block.form.y]
	self.biome = block.biome if block.biome else level.get_random_biome()

	# add miniblocks according to tile form
	for i in range(self.form.x):
		for j in range(self.form.y):
			var miniblock_position = Vector3(i * self.form.scale, 0, j * self.form.scale)
			var miniblock = MiniBlock.new(level, side, self, miniblock_position, miniblock_position + self.global_position)
