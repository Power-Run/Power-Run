class_name MegaTile
extends VirtualNode


const FORM_SCALE = 64

const ALLOWED_FORMS : Array = [11, 12, 21, 13, 31, 14, 41, 15, 51, 22, 23, 32]


func _init(level : Level, side : Side, block : Block, _form_type, position : Vector3, global_position : Vector3):
	super(_form_type, FORM_SCALE, position, global_position)
	self.name = "Tile_(%d,%d)" % [block.form.x, block.form.y]
	var biome : Biome = level.get_random_biome()
	
	var scenepath : String = "res://world/biomes/%s/minitiles/minitile_%dx%d_1.tscn" % [biome.name, self.form.min, self.form.max]
	var megatile = load(scenepath).instantiate()
	
	# jitter megatile color
	var minitile_color = Helper.jitter_hsv(level.jitter_rng, biome.color, biome.minitile_jitter)
	
	var gt : Transform3D = Transform3D(Basis.IDENTITY, self.global_position)
	for csgbox in megatile.get_children():
		var t : Transform3D = Transform3D.IDENTITY.scaled(csgbox.size).translated(csgbox.position)
		# then possibly rotate and translate wrt offset and add half size correction factor as well as raise
		if self.form.y < self.form.x:
			t = t.rotated(Vector3.UP, -PI/2)
		t = t.translated(Vector3(self.form.x*4, 0, self.form.y*4))
		t = t.scaled(Vector3(8,8,8))
		# truncate huge megatile floors
		if csgbox.position.y < -10:
			t = t.scaled(Vector3(1,0.125,1))
		
		# jitter csgbox color
		var csgbox_color = Helper.jitter_hsv(level.jitter_rng, minitile_color, biome.csgbox_jitter)
		
		side.instance_action(self, gt * t, csgbox_color)
