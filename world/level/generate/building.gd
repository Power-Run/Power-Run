class_name Building
extends VirtualNode


const FORM_SCALE : int = 8
const BUILDING_DENSITY_FACTOR : float = 1
#var info : Array
var variation : int


#func _on_body_entered_minitile(body: Node3D):
#	if body is CharacterBody3D:
#		body.minitile_info = self.info


func _init(level : Level, side : Side, tile : Tile, miniblock : MiniBlock, _form_type : int, position : Vector3, global_position : Vector3):
	super(_form_type, FORM_SCALE, position, global_position)
	self.name = "Building_(%d,%d)" % [miniblock.form.x, miniblock.form.y]

	#self.side = side
	var biome : Biome = tile.biome
	var building_name : String = level.get_random_building_name()
	var scenepath_format : String = "res://world/buildings/%s/minitiles/minitile_%dx%d_%d.tscn"
	
	var scenepath_values = func(variation):
		return [building_name, self.form.min, self.form.max, variation]
	
	# count variations
	var i = 0
	while ResourceLoader.exists(scenepath_format % scenepath_values.call(i+1)):
		i += 1
	# load random building variation with random raise value
	self.variation = level.rng.randi_range(1, i)
	var scenepath : String = scenepath_format % scenepath_values.call(self.variation)
	var building = load(scenepath).instantiate()
	var raise : float = biome.raise_building.get_random_value(level.rng)
	# set color
	var color : Color = Color(0.0, 0.0, 0.0)
	# loop over CSGBoxes in building
	var gt : Transform3D = Transform3D(Basis.IDENTITY, self.global_position)
	for csgbox in building.get_children():
		# first scale and translate wrt position in minitile
		var t : Transform3D = Transform3D.IDENTITY.scaled(csgbox.size).translated(csgbox.position)
		# then possibly rotate and translate wrt offset and add half size correction factor as well as raise
		if self.form.y < self.form.x:
			t = t.rotated(Vector3.UP, -PI/2)
		t = t.translated(Vector3(self.form.x*4, raise, self.form.y*4))
		# 1st pass: count CSGBOX / 2nd pass: create CSGBox
		side.instance_action(self, gt * t, color)
