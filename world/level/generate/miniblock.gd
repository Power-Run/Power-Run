class_name MiniBlock
extends VirtualNode


const FORM_TYPE = 88
const FORM_SCALE = 8
#const CONSTRUCT_BUILDINGS_PROBABILITY = 0.33

func _make_density_urn(riker_count: int):
	var array : Array[int] = []
	for i in range(11):
		array.append(1 if i < riker_count else 0)
	return Helper.Urn.new(array)


func _init(level : Level, side : Side, tile : Tile, position : Vector3, global_position : Vector3):
	super(FORM_TYPE, FORM_SCALE, position, global_position)
	self.name = "Miniblock_(%d,%d)" % [tile.form.x, tile.form.y]
	var biome : Biome = tile.biome
	var mask : Array = level.get_random_mask()
	var full_tile_number : int = biome.density.get_random_value(level.rng) 
	var density_urn : Helper.Urn = _make_density_urn(full_tile_number)
	var alien_tile_number : int = biome.alien_density.get_random_value(level.rng) 
	var alien_density_urn : Helper.Urn = _make_density_urn(alien_tile_number)
	var buildings_tile_number : int = biome.buildings_density.get_random_value(level.rng)
	var allowed_buildings_urn : Helper.Urn = Helper.Urn.new(biome.allowed_buildings.duplicate())
	for i in range(max(0, biome.allowed_buildings.size()-buildings_tile_number)):
		allowed_buildings_urn.randi_pop(level.rng)

	# add minitiles according to mondrian mask
	var seen_minitiles : Dictionary = {}
	for i in range(self.form.x):
		for j in range(self.form.y):
			var _form_type : int = mask[i][j]
			if not seen_minitiles.has(_form_type):
				var minitile : VirtualNode
				var minitile_position = Vector3(i * self.form.scale, 0, j * self.form.scale)
				#if do_construct_buildings and not level.building_names.is_empty() and (_form_type in allowed_buildings_urn.array or Helper.form_twin(_form_type) in allowed_buildings_urn.array):
				if not level.building_names.is_empty() and (_form_type in allowed_buildings_urn.array or Helper.form_twin(_form_type) in allowed_buildings_urn.array):
					# make Building with probability 2.1% if form is one of [2x3, 2x4, 2x5, 3x3, 3x4]
					minitile = Building.new(level, side, tile, self, _form_type, minitile_position, minitile_position + self.global_position)
				else:
					# make Minitile elsewise
					# minitile = MiniTile.new(self, _offset + Vector3(self.scale_factor*j, 0, self.scale_factor*i), form, orientation, density.pop_random())
					minitile = MiniTile.new(level, side, tile, self, _form_type, minitile_position, minitile_position + self.global_position, density_urn.randi_pop(level.rng), alien_density_urn.randi_pop(level.rng))
				#add_child(minitile)
				seen_minitiles[_form_type] = minitile
