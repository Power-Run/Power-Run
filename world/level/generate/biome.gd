class_name Biome
extends Object


var name : String
var config : ConfigFile = ConfigFile.new()

var color : Color
var minitile_jitter : Dictionary = {"hue_intensity": 0, "hue_type": 1, "saturation_intensity": 0,"saturation_type": 1, "value_intensity": 0, "value_type": 1}
var csgbox_jitter : Dictionary = {"hue_intensity": 0, "hue_type": 1, "saturation_intensity": 0,"saturation_type": 1, "value_intensity": 0, "value_type": 1}
var raise_empty : Helper.WeightDict
var raise_full : Helper.WeightDict
var raise_building : Helper.WeightDict
var density : Helper.WeightDict
var alien_density : Helper.WeightDict
var buildings_density : Helper.WeightDict
var allowed_buildings : Array


func _init(_name : String):
	self.name = _name
	print(self.name)
	self.config.load("res://world/biomes/" + _name + "/biome.cfg")
	self.color = Color(config.get_value("biome", "color", "#000000"))
	var _minitile_jitter = config.get_value("minitile", "jitter", {})
	var _csgbox_jitter = config.get_value("csgbox", "jitter", {})
	self.minitile_jitter.merge(_minitile_jitter, true)
	self.csgbox_jitter.merge(_csgbox_jitter, true)
	# raise_level -> weight
	self.raise_empty = Helper.WeightDict.new(config.get_value("minitile", "raise_empty", {0:1}))
	self.raise_full = Helper.WeightDict.new(config.get_value("minitile", "raise_full", {0:1}))
	self.raise_building = Helper.WeightDict.new(config.get_value("minitile", "raise_building", {0:1}))
	# density -> weight
	self.density = Helper.WeightDict.new(config.get_value("miniblock", "density", {11:1}))
	# aliendensity -> weight
	self.alien_density = Helper.WeightDict.new(config.get_value("miniblock", "alien_density", {0:1}))
	var buildings_density : Dictionary = config.get_value("miniblock", "buildings_density", {})
	if buildings_density.is_empty():
		buildings_density = {0:1}
	if 0 in buildings_density:
		buildings_density[0] /= float(Building.BUILDING_DENSITY_FACTOR)
	self.buildings_density = Helper.WeightDict.new(buildings_density)
	self.allowed_buildings = config.get_value("miniblock", "allowed_buildings", [])
