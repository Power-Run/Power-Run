class_name Block
extends VirtualNode

var biome : Biome = null
const MONOBLOCK_PROBABILITY : float = 0.03
const FORM_TYPE : int = 88
const FORM_SCALE : int = 64
var is_monoblock : bool = false

# TODO remove unused tags in label.infos
func area_info():
	var _print = func():
		return "%s %s" % [self.name, "(monoblock!)" if self.is_monoblock else ""]
	return {"Block": _print}


func _init(level : Level, side : Side, position : Vector3, global_position : Vector3):
	super(FORM_TYPE, FORM_SCALE, position, global_position)
	var mask : Array = level.get_random_mask()
	var raise_urn : Helper.Urn = Helper.Urn.new(level.get_random_tile_raise().duplicate())
	if level.rng.randf() < MONOBLOCK_PROBABILITY:
		self.is_monoblock = true
		self.biome = level.get_random_biome()
	# probability of allowed forms to become megatiles
	var megatile_probability : float = level.rng.randf_range(0.1,0.2) 
	# add tiles according to mondrian mask
	var seen_tiles : Dictionary = {}
	for i in range(self.form.x):
		for j in range(self.form.y):
			var _form_type : int = mask[i][j]
			if not seen_tiles.has(_form_type):
				var tile : VirtualNode
				var tile_position = Vector3(i * self.form.scale, raise_urn.randi_pop(level.rng), j * self.form.scale)
				if _form_type in MegaTile.ALLOWED_FORMS and level.rng.randf() < megatile_probability:
					# megatiles with probability 80% if 1x1 or 1x2
					tile = MegaTile.new(level, side, self, _form_type, tile_position, tile_position + self.global_position)
				else:
					# normal tiles elsewise
					tile = Tile.new(level, side, self, _form_type, tile_position, tile_position + self.global_position)

				#add_child(tile)
				seen_tiles[_form_type] = tile

	# add an Area3D for debugging
	#var area : FormedNode3DArea3D = FormedNode3DArea3D.new(self)
	#area.enable_mesh(Color(1,0,0), 0.5)
	#add_child(area)
