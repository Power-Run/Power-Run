class_name FormedNode3DArea3D
extends Area3D

var formed_node : VirtualNode
var collision_shape : CollisionShape3D
var mi3d : MeshInstance3D


func _init(_formed_node : VirtualNode):
	self.formed_node = _formed_node
	self.collision_shape = CollisionShape3D.new()
	self.collision_shape.shape = BoxShape3D.new()
	var form : VirtualNode.Form = self.formed_node.form
	self.collision_shape.shape.size = form.scale*Vector3(form.x, 1.2, form.y)


func _on_body_entered_area3d(body: Node3D):
	if body is Player:
		var label : lblFPS = find_parent("Level").get_node("InfoLabel")
		label.infos.merge(self.formed_node.area_info(), true)


#match (find_parent("Side*").name):
		#"Side_xz":
			#shape.size = FORM_SCALE*Vector3(self.form.x, 3, self.form.y)
			#area.position = shape.size/2
			#self.area.position += Vector3(0,raise-1,0)
		#"Side_yx":
			##shape.size = Vector3(FORM_SCALE*self.form.y, FORM_SCALE*self.form.x ,3*FORM_SCALE)
			#shape.size = FORM_SCALE*Vector3(3, self.form.x, self.form.y)
			#area.position = shape.size/2
			#self.area.position += Vector3(0,0,raise-1)
		#"Side_zy":
			#shape.size = FORM_SCALE*Vector3(self.form.x, self.form.y, 3)
			#area.position = shape.size/2
			#self.area.position += Vector3(raise-1,0,0)


func enable_mesh(_base_color:Color, _transparency:float):
	self.mi3d = MeshInstance3D.new()
	self.mi3d.scale = self.collision_shape.shape.size
	##self.mi3d.position = shape.position
	var mat : Material = StandardMaterial3D.new()
	mat.vertex_color_use_as_albedo = true
	mat.albedo_color = _base_color
	self.mi3d.transparency = _transparency
	self.mi3d.mesh = preload("res://resources/box-0.5.obj").duplicate()
	self.mi3d.mesh.surface_set_material(0, mat)
	add_child(self.mi3d)


func disable_mesh():
	remove_child(self.mi3d)


# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(self.collision_shape)

	self.monitoring = true
	self.monitorable = false
	self.body_entered.connect(_on_body_entered_area3d)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
