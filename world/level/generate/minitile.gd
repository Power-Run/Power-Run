class_name MiniTile
extends VirtualNode


#var info : Array
var is_full : int
var is_alien : int
var variation : int
var area : FormedNode3DArea3D

const FORM_SCALE : int = 8
const ALIEN_COLOR_LERP_WEIGHT : float = 0.2


func display_info():
	return "minitile_%dx%d_%d %s" % [self.form.min, self.form.max, self.variation, "(alien!)" if self.is_alien else ""]


func _init(level : Level, side : Side, tile : Tile, miniblock : MiniBlock, _form_type : int, position : Vector3, global_position : Vector3, _is_full: int, _is_alien: int):
	super(_form_type, FORM_SCALE, position, global_position)
	self.name = "Minitile_(%d,%d)" % [miniblock.form.x, miniblock.form.y]
	self.is_full = _is_full
	self.is_alien = _is_alien if _is_full else false
	var biome : Biome = level.get_random_biome() if self.is_alien else tile.biome
	var scenepath_format : String = "res://world/biomes/%s/minitiles/minitile_%dx%d_%d.tscn"
	var scenepath_values = func(variation):
		return [biome.name, self.form.min, self.form.max, variation]

	if not self.is_full:
		self.variation = 0
	else: 
		var i = 0
		while ResourceLoader.exists(scenepath_format % scenepath_values.call(i+1)):
			i += 1
		self.variation = level.rng.randi_range(1, i)
	var scenepath : String = scenepath_format % scenepath_values.call(self.variation)

	var minitile = load(scenepath).instantiate()
	var raise : float = (biome.raise_full if self.is_full else biome.raise_empty).get_random_value(level.rng)

	# load and jitter minitile color
	var base_color = tile.biome.color.lerp(biome.color, ALIEN_COLOR_LERP_WEIGHT)
	var minitile_color = Helper.jitter_hsv(level.jitter_rng, base_color, biome.minitile_jitter)

	# create minitile info
	#var info : MiniTileInfo = MiniTileInfo.new()

	# create minitile area
	#self.area = MiniTileArea3D.new(self, [info])
	#add_child(self.area)
	# loop over CSGBoxes in minitile scene
	var gt : Transform3D = Transform3D(Basis.IDENTITY, self.global_position)
	for csgbox in minitile.get_children():
		# first scale and translate wrt position in minitile
		var t : Transform3D = Transform3D.IDENTITY.scaled(csgbox.size).translated(csgbox.position)
		# then possibly rotate and translate wrt offset and add half size correction factor as well as raise
		if self.form.y < self.form.x:
			t = t.rotated(Vector3.UP, -PI/2)
		t = t.translated(Vector3(self.form.x*4, raise, self.form.y*4))
		
		# jitter csgbox color
		var csgbox_color = Helper.jitter_hsv(level.jitter_rng, minitile_color, tile.biome.csgbox_jitter)

		# 1st pass: count CSGBOX / 2nd pass: create CSGBox
		side.instance_action(self, gt * t, csgbox_color)
