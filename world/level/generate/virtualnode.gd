class_name VirtualNode
extends Object


var form : Form
var position : Vector3
var global_position : Vector3
var name : String

class Form:
	var type : int
	var scale : int
	var x : int
	var y : int
	var min : int
	var max : int
	func _init(_type, _scale):
		self.type = _type
		self.scale = _scale
		self.x = _type % 10
		self.y = (_type - self.x) / 10
		self.min = min(x, y)
		self.max = max(x, y)


func _init(_form_type : int, _form_scale : int, position : Vector3, global_position : Vector3):
	self.form = Form.new(_form_type, _form_scale)
	self.position = position
	self.global_position = global_position


func display_info():
	return self.get_class()


#func add_child(node: Node, force_readable_name: bool = false, internal: int = 0) -> void:
#	# entweder add_child to multimesh
#	# oder normales add_child
#	pass
