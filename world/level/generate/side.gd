extends Node3D
class_name Side

enum PassType {PASS_NULL, PASS_COUNT, PASS_ADD}


class MultiMeshData:
	var instance_count : int = 0
	var multimesh : MultiMesh
	var shape : Shape3D
	var collisionNode : StaticBody3D
	var mesh : Mesh
	var material : BaseMaterial3D
	var uses_colors : bool

var mmdata : MultiMeshData = MultiMeshData.new()
var building_mmdata : MultiMeshData = MultiMeshData.new()

const FORM_SCALE : int = 512
var pass_type : PassType = PassType.PASS_NULL

var coordinates : Array


func instance_action(_f: VirtualNode, _t : Transform3D, _c: Color):
#	print("instance action")
	var _mmdata : MultiMeshData = self.mmdata if not _f.name.begins_with("Building_") else self.building_mmdata
	match (self.pass_type):
		PassType.PASS_COUNT:
			_mmdata.instance_count += 1
		PassType.PASS_ADD:
			_mmdata.multimesh.set_instance_transform(_mmdata.instance_count, _t)
			if _mmdata.multimesh.use_colors == true:
				_mmdata.multimesh.set_instance_color(_mmdata.instance_count, _c)
			_mmdata.instance_count += 1
			# add collision
			var collisionShape = CollisionShape3D.new()
			collisionShape.shape = _mmdata.shape
			collisionShape.transform = _t
			_mmdata.collisionNode.add_child(collisionShape)


func _make_multimesh(_mmdata : MultiMeshData) -> MultiMesh:
	# create MultiMesh
	var mm = MultiMesh.new()
	_mmdata.collisionNode = StaticBody3D.new()
	mm.transform_format = MultiMesh.TRANSFORM_3D
	mm.use_colors = _mmdata.uses_colors
	# set instance count
	mm.instance_count = _mmdata.instance_count
	# set box object
	mm.mesh = _mmdata.mesh
	# set material
	var mat : BaseMaterial3D = _mmdata.material
	mat.albedo_color = Color(1, 1, 1)
	mat.vertex_color_use_as_albedo = true
	mm.mesh.surface_set_material(0, mat)
	# create collision shape and node
	_mmdata.shape = mm.mesh.create_trimesh_shape()
	add_child(_mmdata.collisionNode)
	return mm


func _init(level : Level, _coordinates : Array, position : Vector3):
	self.coordinates = _coordinates
	self.position = position
	mmdata.mesh = preload("res://resources/box-0.5.obj")
	# assign material
	mmdata.material = preload("res://textures/grid.tres")
	# to test buggy material
	#mmdata.material = StandardMaterial3D.new()
	mmdata.uses_colors = true
	building_mmdata.mesh = preload("res://resources/building_box-0.5.obj")
	building_mmdata.material = preload("res://textures/building_grid.tres")
	building_mmdata.uses_colors = false
	for coord in self.coordinates:
		var i : int = coord.x
		var j : int = coord.y
		# save state
		var state = level.rng.state
		mmdata.instance_count = 0
		building_mmdata.instance_count = 0
		# 1st pass
		self.pass_type = PassType.PASS_COUNT
		print("counting...")
		var position1 : Vector3 = Vector3(i * FORM_SCALE, 0, j * FORM_SCALE)
		var block1 = Block.new(level, self, position1, position1 + self.position)
		block1.name = "Block1_(%d,%d)" % [i, j]
		print("counted %d minitile boxes, %d building boxes" % [mmdata.instance_count, building_mmdata.instance_count])
		
		var mmi = MultiMeshInstance3D.new()
		mmdata.multimesh = _make_multimesh(mmdata)
		
		var building_mmi = MultiMeshInstance3D.new()
		building_mmdata.multimesh = _make_multimesh(building_mmdata)
		
		# load state
		level.rng.state = state
		mmdata.instance_count = 0
		building_mmdata.instance_count = 0
		# 2nd pass
		self.pass_type = PassType.PASS_ADD
		print("adding...")
		var position2 = Vector3(i * FORM_SCALE, 0, j * FORM_SCALE)
		var block2 = Block.new(level, self, position2, position2 + self.position)
		block2.name = "Block_(%d,%d)" % [i, j]
		print("added %d minitile boxes, %d building boxes" % [mmdata.instance_count, building_mmdata.instance_count])
		
		mmi.multimesh = mmdata.multimesh
		add_child(mmi)

		building_mmi.multimesh = building_mmdata.multimesh
		add_child(building_mmi)

		Global.load_block_inc.emit()
