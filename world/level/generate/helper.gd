class_name Helper
extends Object


class Urn:
	var array : Array
	
	func _init(_array):
		self.array = _array
	
	func add(value):
		self.array.append(value)

	func randi_pop(rng : RandomNumberGenerator):
		return self.array.pop_at(rng.randi_range(0, len(self.array) - 1))


class WeightDict:
	var intervals : Array = []
	var values : Array = []
	var total : float = 0
	func _init(dict : Dictionary):
		var current = 0
		intervals.append(current)
		for key in dict:
			current += dict[key]
			self.intervals.append(current)
			self.values.append(key)
		self.total = current
	func get_value(point : float):
		# TODO check empty dictionary
		for i in range(len(self.intervals)-1):
			if self.intervals[i] < point and point < self.intervals[i+1]:
				return self.values[i]
	func get_random_value(rng : RandomNumberGenerator):
		return get_value(rng.randf_range(0, self.total))


static func get_subdirectory_list(path) -> Array:
	var result : Array = []
	var dir : DirAccess = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var filename = dir.get_next()
		while filename != "":
			if dir.current_is_dir():
				result.append(filename)
			filename = dir.get_next()
	result.sort()
	return result


static func jitter(rng:RandomNumberGenerator, value:float, intensity:float, type:int, wrap:bool=false) -> float:
	value *= 1 - intensity
	for i in range(type):
		value += intensity/type * (rng.randf() - float(wrap)/2)
	return value


static func jitter_hsv(rng:RandomNumberGenerator, base_color:Color, jitter_dict:Dictionary) -> Color:
	var color : Color = base_color
	color.h = jitter(rng, base_color.h, jitter_dict.hue_intensity, jitter_dict.hue_type, true)
	color.s = jitter(rng, base_color.s, jitter_dict.saturation_intensity, jitter_dict.saturation_type)
	color.v = jitter(rng, base_color.v, jitter_dict.value_intensity, jitter_dict.value_type)
	return color


static func form_twin(_type:int) -> int:
	var x = _type % 10
	var y = (_type - x) / 10
	return 10*x + y

static func erf(x:float) -> float:
	# erf is an odd function but the algorithm expects a positive x
	if x<0:
		return -erf(-x)
	
	# constants
	var a1 : float =  0.254829592 
	var a2 : float = -0.284496736
	var a3 : float =  1.421413741 
	var a4 : float = -1.453152027
	var a5 : float =  1.061405429
	var p  : float =  0.3275911
	
	# A & S 7.1.26 (reference book)
	var t : float = 1.0/(1.0 + p*x)
	return 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x)

static func phi(x:float) -> float:
	return exp(-0.5*x*x)/sqrt(2*PI)

static func Phi(x:float) -> float:
	return (1 + erf(x/sqrt(2)))/2

static func cumulative_truncated_normal_distribution(x:float, µ:float, σ:float) -> float:
	var a : float = 0
	var b : float = 1
	return (Phi((x-µ)/σ) - Phi((a-µ)/σ))/(Phi((b-µ)/σ) - Phi((a-µ)/σ))
