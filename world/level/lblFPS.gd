class_name lblFPS
extends Label

var infos : Dictionary = {
		"FPS" : func(): return "FPS: %d" % Engine.get_frames_per_second(),
		"Speed" : func(): return "Speed: %.3f" % %Player.speed(),
		"HSpeed" : func(): return "HSpeed: %.3f" % %Player.hspeed(),
		"VSpeed" : func(): return "VSpeed: %.3f" % %Player.vspeed(),
		"Impact" : func(): return "impact: %.3f" % %Player._last_impact_speed,
		#"MovementState" : func(): return "STATE movement: %s" % %Player.state_machine.movement.name,
		#"WeaponState" : func(): return "STATE weapon: %s" % %Player.state_machine.weapon.name,
		#"PostureState" : func(): return "STATE posture: %s" % %Player.state_machine.posture.name,
		#"CameraState" : func(): return "STATE camera: %s" % %Player.state_machine.camera.name,
		#"BREATHING_PITCH" : func(): return "Breathing pitch: %.3f" % %Player.head.breathing_pitch,
		#"BREATING_YAW" : func(): return "Breathing yaw: %.3f" % %Player.head.breathing_yaw,
		#"BOBBING_PITCH" : func(): return "Bobbing pitch: %.3f" % %Player.head.bobbing_pitch,
		#"BOBBING_YAW" : func(): return "Bobbing yaw: %.3f" % %Player.head.bobbing_yaw
	}


func _init():
	self.name = "InfoLabel"


func _ready() -> void:
	self.owner = $"/root" 


func _process(_delta: float) -> void:
	set_text("\n".join(self.infos.values().map(func(f): return f.call())))
