class_name Game
extends Control


@export var VERSION : String = "0.1.4"

var settings : ConfigFile = ConfigFile.new()
@onready var main_menu : HBoxContainer = %hboxMainMenu
var level : Level
var skybox : Node3D
var is_world : bool = false

signal save_settings()
signal load_settings()

func _init() -> void:
	# randomize global seed
	randomize()
	# load GUI settings
	settings.load("user://settings.cfg")
	# clear seed history
	#settings.set_value("GUI", "seed_history", [])


func _enter_tree() -> void:
	self.load_settings.emit()


func _ready() -> void:
	%svboxLevel.visible = false
	%svboxSkybox.visible = false
	self.load_settings.emit()


# enable quit by pressing esc
func _unhandled_input(_event) -> void:
	if Input.is_action_just_pressed("quit"):
		if self.is_world:
			add_main_menu()
		else:
			self.save_settings.emit()
			settings.save("user://settings.cfg")
			get_tree().quit()


func add_world_scenes(level : Level, skybox : Node3D) -> void:
	self.level = level
	self.skybox = skybox
	self.is_world = true
	
	# add level
	%Level.replace_by(level)
	level.name = "Level"
	# make %Level accessible
	level.owner = $"/root"
	level.unique_name_in_owner = true
	level.get_viewport().get_parent().visible = true
	
	# add skybox
	%Skybox.replace_by(skybox)
	skybox.name = "Skybox"
	# make %Skybox accessible
	skybox.owner = $"/root"
	skybox.unique_name_in_owner = true
	skybox.get_viewport().get_parent().visible = true
	
	# remove main menu
	remove_child(main_menu)


func add_main_menu() -> void:
	self.is_world = false
	
	# remove level 
	var level_placeholder : Node3D = Node3D.new()
	level.get_viewport().get_parent().visible = false
	level.replace_by(level_placeholder)
	level_placeholder.name = "Level"
	# make %Level accessible
	level_placeholder.owner = $"/root"
	level_placeholder.unique_name_in_owner = true
	
	# remove skybox
	var skybox_placeholder : Node3D = Node3D.new()
	skybox.get_viewport().get_parent().visible = false
	skybox.replace_by(skybox_placeholder)
	skybox_placeholder.name = "Skybox"
	# make %Skybox accessible
	skybox_placeholder.owner = $"/root"
	skybox_placeholder.unique_name_in_owner = true
	
	# add main menu
	add_child(main_menu)
