extends Node

var orientations : Orientations = Orientations.new()

# SIGNALS
signal load_block(total : int)
signal load_block_inc()
