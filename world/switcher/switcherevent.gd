class_name PlaneSwitchEvent
extends Node

@export var BODY : Node3D
@export var TARGET_NORMAL : Vector3 = Vector3.DOWN
@export var WAIT_TIME : float
@export var SWITCH_TIME : float

enum Phase {WAITING, SWITCHING, COMPLETED}

# Class Variables
var current_phase : Phase
var wait_timer : Timer = Timer.new()
var switch_timer : Timer = Timer.new()


func _init(body: Node3D, target_normal: Vector3, wait_time: float, switch_time: float):
	self.BODY = body
	self.WAIT_TIME = wait_time
	self.SWITCH_TIME = switch_time
	self.TARGET_NORMAL = target_normal
	self.BODY.add_child(self)


func get_current_phase() -> Phase:
	return self.current_phase


func get_wait_timer() -> Timer:
	return self.wait_timer


func get_switch_timer() -> Timer:
	return self.switch_timer


func start() -> void:
	self.current_phase = Phase.WAITING
	wait_timer.start()


func _on_wait_timer_timeout() -> void:
	self.current_phase = Phase.SWITCHING
	self.switch_timer.start()


func _on_switch_timer_timeout() -> void:
	self.BODY.up_direction = -self.TARGET_NORMAL			# FIXME make moment of setting new TARGET_NORMAL completely choosable
	self.current_phase = Phase.COMPLETED


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#var e : Environment = self.BODY.get_node("head/Camera").environment
	#source_sky_rotation = e.sky_rotation
	# Set up wait timer
	self.add_child(wait_timer)
	wait_timer.one_shot = true
	wait_timer.wait_time = self.WAIT_TIME
	wait_timer.timeout.connect(_on_wait_timer_timeout)
	# Set up switch timer
	self.add_child(switch_timer)
	self.switch_timer.one_shot = true
	self.switch_timer.wait_time = self.SWITCH_TIME
	self.switch_timer.timeout.connect(_on_switch_timer_timeout)
	self.switch_timer.process_callback = Timer.TimerProcessCallback.TIMER_PROCESS_PHYSICS


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta) -> void:
	if self.current_phase == Phase.SWITCHING:
		var b : Node3D = self.BODY
		# get environment vom camera for rotation
		#var e : Environment = b.get_node("head/Camera").environment
		# Calculate rotation percentage, axis and angle
		var amount = clamp(delta/max(switch_timer.time_left, delta), 0, 1)
		var axis : Vector3 = b.gravity_direction.cross(self.TARGET_NORMAL)
		var angle : float = b.gravity_direction.angle_to(self.TARGET_NORMAL)
		# Rotate local coordinate system, gravity/up direction by amount
		b.global_transform.basis = b.global_transform.basis.rotated(axis.normalized(), angle * amount)
		b.gravity_direction = b.gravity_direction.slerp(self.TARGET_NORMAL, amount)
		b.up_direction = b.up_direction.slerp(-self.TARGET_NORMAL, amount)
		# rotate sky
		#var sky_axis = self.source_normal.cross(self.TARGET_NORMAL)
		#e.sky_rotation = e.sky_rotation.slerp(PI/2 * sky_axis.normalized(), amount)
