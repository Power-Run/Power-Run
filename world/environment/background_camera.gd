class_name BackgroundCamera
extends Camera3D


func _ready() -> void:
	# make %Player accessible
	self.owner = $"/root"
	%Player.camera.updated.connect(_update)


func _update(camera : Camera3D):
	self.rotation = camera.global_rotation
	self.fov = camera.fov
