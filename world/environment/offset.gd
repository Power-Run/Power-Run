extends Node3D

var ALLOWED_FORMS : Array = [[1,4],[1,5],[2,2],[2,3],[2,4],[2,5],[3,3],[3,4],[2,4],[2,5],[3,3],[3,4],[2,4],[2,5],[3,3],[3,4]]


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# make %Level accessible
	self.owner = $"/root"
	var level : Level = %Level
	var biome : Biome = level.get_random_biome()
	var form : Array = ALLOWED_FORMS[level.rng.randi() % ALLOWED_FORMS.size()]
	var scenepath_format : String = "res://world/biomes/%s/minitiles/minitile_%dx%d_%d.tscn"
	var scenepath_values = func(variation):
		return [biome.name, form[0], form[1], variation]

	var i = 0
	while ResourceLoader.exists(scenepath_format % scenepath_values.call(i+1)):
		i += 1
	var variation = level.rng.randi_range(1, i)
	var scenepath : String = scenepath_format % scenepath_values.call(variation)
	
	var minitile = load(scenepath).instantiate()
	add_child(minitile)
